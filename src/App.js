import React, {useState} from 'react';
import {nanoid} from "nanoid";
import BurgerBlock from "./Components/BurgerBlock/BurgerBlock";
import IngredientsControls from "./Components/IngredientsControls/IngredientsControls.js";
import meatImage from './assets/meat.png';
import cheeseImage from './assets/cheese.png';
import saladImage from './assets/salad.png';
import baconImage from './assets/bacon.png';
import './App.css';

const INGREDIENTS = [
  {key:nanoid(), name: 'Meat', price: 50, image: meatImage},
  {key:nanoid(), name: 'Cheese', price: 20, image: cheeseImage},
  {key:nanoid(), name: 'Salad', price: 5, image: saladImage},
  {key:nanoid(), name: 'Bacon', price: 30, image: baconImage},
];

const App = () => {
    const [ingredients, setIngredients] = useState(INGREDIENTS.map(item => {
        return  {key: item.key, name: item.name, count: 0};
    }));
    const [cost, setCost] = useState(20);

    const layerCost = name => {
        let cost = 0;
        INGREDIENTS.forEach(item => {
            if (item.name === name) {
                cost = item.price;
            }
        })
        return cost;
    }

    const addLayer = e => {
        const name = e.target.name;
        setIngredients(ingredients.map(item => {
            if(item.name === name) {
                item.count++;
            }
            return item;
        }));
        setCost(cost + layerCost(name));
    }

    const removeLayer = e => {
        const name = e.target.name;
        setIngredients(ingredients.map(item => {
            if(item.name === name) {
                item.count--;
            }
            return item;
        }));
        setCost(cost - layerCost(name));
    }

    return (
        <div className="App container">
            <div className="col left-col">
                <IngredientsControls ingredients={ingredients} ingredients_count={INGREDIENTS} addLayer={addLayer} removeLayer={removeLayer} />
            </div>
            <div className="col right-col">
                <BurgerBlock ingredients={ingredients} cost={cost} />
            </div>
        </div>
    );
}

export default App;