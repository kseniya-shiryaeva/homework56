import React from 'react';
import './Burger.css';
import IngredientLayer from "../IngredientLayer/IngredientLayer";

const layers = ingredients => {
    const result = [];
    ingredients.forEach(item => {
        result.push(<IngredientLayer name={item.count} />);
        for (let i = 0; i < item.count; i++) {
            result.push(<IngredientLayer name={item.name} />);
        }
    })
    return result;
}

const Burger = ({ingredients}) => {
    return (
        <div className="Burger">
            <div className="BreadTop">
                <div className="Seeds1"> </div>
                <div className="Seeds2"> </div>
            </div>
            {layers(ingredients)}
            <div className="BreadBottom"> </div>
        </div>
    );
};

export default Burger;