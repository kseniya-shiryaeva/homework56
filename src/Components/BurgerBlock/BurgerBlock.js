import React from 'react';
import Burger from "../Burger/Burger";
import Price from "../Price/Price";

const BurgerBlock = ({ingredients, cost}) => {
    return (
        <fieldset className="burger-block">
            <legend>Burger</legend>
            <Burger ingredients={ingredients} />
            <Price cost={cost} />
        </fieldset>
    );
};

export default BurgerBlock;