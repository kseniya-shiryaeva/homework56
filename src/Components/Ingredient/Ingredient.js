import React from 'react';
import Urn from "../../assets/trash-alt-regular.svg";
import './Ingredient.css';

const removeButton = (name, number, removeLayer) => {
    if (number > 0) {
        return <div><img src={Urn} alt="Remove" className="removeLayer" name={name} onClick={removeLayer} /></div>;
    }
    return  <div> </div>;
}

const Ingredient = ({image, name, count, addLayer, removeLayer}) => {
    return (
        <div className="Ingredient">
            <span>
                <img src={image} alt={name} className="ingredientImage" name={name} onClick={addLayer} />
            </span>
            <span>{name}</span>
            <span>x {count}</span>
            <span>{removeButton(name, count, removeLayer)}</span>
        </div>
    );
};

export default Ingredient;