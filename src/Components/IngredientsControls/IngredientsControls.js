import React from 'react';
import Ingredient from "../Ingredient/Ingredient";
import './IngredientsControls.css';

const IngredientsControls = ({ingredients, ingredients_count, addLayer, removeLayer}) => {

    return (
        <fieldset className="ingredients-block">
            <legend>Ingredients</legend>
            {
                ingredients.map((item, key) => (
                    <Ingredient image={ingredients_count[key].image} name={item.name} count={item.count} addLayer={addLayer} removeLayer={removeLayer} />
                ))
            }
        </fieldset>
    );
};

export default IngredientsControls;