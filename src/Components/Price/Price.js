import React from 'react';
import './Price.css';

const Price = ({cost}) => {
    return (
        <div className="Price">Price: <b>{cost} сом</b></div>
    );
};

export default Price;